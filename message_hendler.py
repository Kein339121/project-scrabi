from initialization import (
    bot,
    owm,
    db
)
from bot_files.keyboard_parser import (
    city_keyboards,
    opponent_keiboards,
    opponent_wait_keyboard,
    city_game_keyboard,
    draw_keyboard
)
from bot_files.comands import COMMANDS
from bot_files.mesages import MESSAGES


def send_weather(chat_id, text):
    if text.lower() == 'назад':
        answer = MESSAGES['default']
        state = 0

    elif owm.check_city(text):
        db.update_latest_places(
            user_id=chat_id,
            place=text
        )
        clouds, wind_speed, hum, temp, f_like, rains = owm.return_weather(text)
        answer = MESSAGES['weather'].format(
            place=text,
            clouds=clouds,
            wind_speed=wind_speed,
            hum=hum,
            temp=temp,
            f_like=f_like,
            rains=rains
        )
        state = 0
    else:
        answer = MESSAGES['wrong_city']
        state = 1
    bot.send_message(
        chat_id=chat_id,
        text=answer,
        state=state
    )


def draw_accept(chat_id, text):
    if text == "ничья":
        opponent = db.get_opponent_id(chat_id)[0]
        try:
            used_u = db.get_used_places(chat_id)[0]
            used_o = db.get_used_places(opponent)[0]
        except:
            used_u = [0]
            used_o = [0]
        bot.send_message(
            chat_id=chat_id,
            text=MESSAGES['draw_accepted'].format(score=len(used_u)),
            state=0
        )
        bot.send_message(
            chat_id=opponent,
            text=MESSAGES['draw_accepted'].format(score=len(used_o)),
            state=0
        )
        db.clear_used_places(chat_id)
        db.clear_used_places(opponent)
        db.set_latest_letter(chat_id, None)
        db.set_latest_letter(opponent, None)
        db.set_opponent_id(chat_id, None)
        db.set_opponent_id(opponent, None)
    elif text == 'отмена':
        opponent = db.get_opponent_id(chat_id)[0]
        bot.send_message(
            chat_id=opponent,
            text=MESSAGES['draw_delay'],
            state=2,
            keyboard=city_game_keyboard()
        )
        bot.send_message(
            chat_id=chat_id,
            text=MESSAGES['draw_delay'],
            state=5,
            keyboard=city_game_keyboard()
        )
    else:
        bot.send_message(
            chat_id=chat_id,
            text=MESSAGES['draw_wrong'],
            state=6,
            keyboard=draw_keyboard(),
        )


def cities_game(chat_id, text:str):
    text = text.lower()
    if text == 'сдаться':
        opponent = db.get_opponent_id(chat_id)[0]
        try:
            used_u = db.get_used_places(chat_id)[0]
            used_o = db.get_used_places(opponent)[0]
        except:
            used_u = [0]
            used_o = [0]
        bot.send_message(
            chat_id=chat_id,
            text=MESSAGES['you_loose'].format(score=len(used_u)),
            state=0
        )
        bot.send_message(
            chat_id=opponent,
            text=MESSAGES['you_win'].format(score=len(used_o)),
            state=0
        )
        db.clear_used_places(chat_id)
        db.clear_used_places(opponent)
        db.set_latest_letter(chat_id, None)
        db.set_latest_letter(opponent, None)
        db.set_opponent_id(chat_id, None)
        db.set_opponent_id(opponent, None)
    elif text == 'ничья':
        opponent = db.get_opponent_id(chat_id)[0]
        bot.send_message(
            chat_id=chat_id,
            text=MESSAGES['wait_opponent'],
            state=6
        )
        bot.send_message(
            chat_id=opponent,
            text=MESSAGES['draw'],
            keyboard=draw_keyboard(),
            state=6
        )
    else:
        opponent = db.get_opponent_id(chat_id)[0]
        try:
            used_u = db.get_used_places(chat_id)
            used_o = db.get_used_places(opponent)
        except:
            used_u = [0]
            used_o = [0]
        print(used_u)
        print(used_o)
        if text in used_u:
            bot.send_message(
                chat_id=chat_id,
                text=MESSAGES['used'].format(
                    city=text,
                    by_who='вами'
                ),
                state=2
            )
        elif text in used_o:
            bot.send_message(
                chat_id=chat_id,
                text=MESSAGES['used'].format(
                    city=text,
                    by_who='оппонентом'
                ),
                state=2
            )
        else:
            if owm.check_city(text):
                try:
                    last_letter = db.get_latest_letter(opponent)[0]
                except:
                    last_letter = text[0]
                if last_letter is None:
                    last_letter = text[0]
                if last_letter == "":
                    last_letter = text[0]
                if last_letter == " ":
                    last_letter = text[0]
                if last_letter == text[0]:
                    db.update_used_places(chat_id, text)
                    temp = text
                    text = text.replace('ь', '')
                    text = text.replace('ъ', '')
                    text = text.replace('э', '')
                    text = text.replace('ы', '')
                    text = text.replace('й', '')
                    db.set_latest_letter(chat_id, text[-1])
                    bot.send_message(
                        chat_id=chat_id,
                        text=MESSAGES['correct_city'].format(
                            city=temp,
                            score=len(used_u)
                        ),
                        state=5,
                        keyboard=city_game_keyboard()
                    )
                    bot.send_message(
                        chat_id=opponent,
                        text=MESSAGES['your_turn'].format(
                            city=temp,
                        ),
                        state=2,
                        keyboard=city_game_keyboard()
                    )
                else:
                    bot.send_message(
                        chat_id=chat_id,
                        text=MESSAGES['wrong_letter'].format(last_letter=last_letter),
                        state=2,
                        keyboard=city_game_keyboard()
                    )
            else:
                bot.send_message(
                    chat_id=chat_id,
                    text=MESSAGES['wrong_city'],
                    state=2,
                    keyboard=city_game_keyboard()
                )


def find_opponent(chat_id, text):
    if text == 'назад':
        bot.send_message(
            chat_id=chat_id,
            text=MESSAGES['default'],
            state=0
        )
    else:
        opponent_state = db.check_state(text)[0]
        if opponent_state == 3:
            bot.send_message(
                chat_id=chat_id,
                text=MESSAGES['wait_opponent'],
                state=4
            )
            db.set_opponent_id(
                user_id=chat_id,
                opponent_id=text
            )
            bot.send_message(
                chat_id=text,
                text=MESSAGES['oppoent_wait'].format(opponent_id=chat_id),
                state=4,
                keyboard=opponent_wait_keyboard(chat_id)
            )
            db.set_opponent_id(
                user_id=text,
                opponent_id=chat_id
            )

        else:
            bot.send_message(
                chat_id=chat_id,
                text=MESSAGES['wrong_opponent'],
                state=3
            )


def accept_opponent(chat_id, text):
    if text == 'принять вызов':
        opponent = db.get_opponent_id(chat_id)[0]
        bot.send_message(
            chat_id=chat_id,
            text=MESSAGES['game_start'].format(
                opponent=opponent,
                first='вами'
            ),
            state=2,
            keyboard=city_game_keyboard()
        )
        bot.send_message(
            chat_id=opponent,
            text=MESSAGES['game_start'].format(
                opponent=chat_id,
                first='оппонентом'
            ),
            state=5,
            keyboard=city_game_keyboard()
        )
    elif text == 'откозаться':
        bot.send_message(
            chat_id=chat_id,
            text=MESSAGES['opponent_skiped'],
            state=3
        )



def message_handler(chat_id, text):
    if text[(len(text)-4):] == "_api":
        bot.send_message(
            chat_id=chat_id,
            text=MESSAGES[text]
        )
    else:
        bot.send_message(
            chat_id=chat_id,
            text=text[::-1],
        )


def command_handler(chat_id, text, name):
    if text == '/start':
        db.add_user(chat_id)
        bot.send_message(
            text=COMMANDS[text].format(name=name),
            chat_id=chat_id
        )
    elif text == '/sub':
        bot.sub_user(user_id=chat_id)
    elif text == '/unsub':
        bot.unsub_user(user_id=chat_id)
    elif text == '/weather':
        cities = db.get_latest_places(chat_id)
        keyboard = city_keyboards(cities)
        if len(cities) != 0:
            text = COMMANDS[text+'1']
        else:
            text = COMMANDS[text]
        bot.send_message(
            text=text,
            chat_id=chat_id,
            keyboard=keyboard,
            state=1
        )
    elif text == '/cities':
        opponents = db.get_opponents()
        bot.send_message(
            text=COMMANDS[text],
            chat_id=chat_id,
            keyboard=opponent_keiboards(chat_id, opponents),
            state=3
        )
    else:
        bot.send_message(
            text=COMMANDS['wrong'],
            chat_id=chat_id
        )


def validate_message(chat_id, text, name=None):
    if text[0] == "/":
        command_handler(
            chat_id=chat_id,
            text=text,
            name=name
        )
    else:
        message_handler(
            chat_id=chat_id,
            text=text,
        )


def sub_unsub(chat_id, text):
    bot.send_message(
        chat_id=chat_id,
        text=MESSAGES[text]
    )