import psycopg2


class Postgresser:
    def __init__(self, db_uri):
        self.connection = psycopg2.connect(db_uri, sslmode='require')
        self.cursor = self.connection.cursor()

    def user_exist(self, user_id):
        with self.connection:
            self.cursor.execute(f"SELECT user_id FROM users WHERE user_id = {user_id}")
            result = self.cursor.fetchone()
            return bool(result)

    def add_user(self, user_id):
        with self.connection:
            self.cursor.execute(f"SELECT user_id FROM users WHERE user_id = {user_id}")
            result = self.cursor.fetchone()
            if not result:
                self.cursor.execute(
                    "INSERT INTO users (user_id, status) VALUES(%s, %s)",
                    (user_id, False)
                )
                self.connection.commit()
                return 'success_add'
            else:
                return 'existed_user'

    def remove_user(self, user_id):
        with self.connection:
            self.cursor.execute(f"SELECT user_id FROM users WHERE user_id = {user_id}")
            result = self.cursor.fetchone()
            if result:
                self.cursor.execute(f"DELETE FROM users WHERE user_id = {user_id}")
                self.connection.commit()
                return 'success_remove'
            else:
                return 'not_existed_user'

    def return_users(self):
        with self.connection:
            self.cursor.execute("SELECT user_id FROM users")
            temp = self.cursor.fetchall()
            return [x for t in temp for x in t]

    def remove_subscription(self, user_id):
        with self.connection:
            self.cursor.execute(f"SELECT status FROM users WHERE user_id = {user_id}")
            result = self.cursor.fetchone()[0]
            print(f'subscription status = {result}')
            if result:
                self.cursor.execute(
                    "UPDATE users SET status = %s WHERE user_id = %s",
                    (False, user_id)
                )
                self.connection.commit()
                return 'success_unsub'
            else:
                return 'not_sub_user'

    def set_subscription(self, user_id):
        with self.connection:
            self.cursor.execute(f"SELECT status FROM users WHERE user_id = {user_id}")
            result = self.cursor.fetchone()[0]
            print(f'subscription status = {result}')
            if not result:
                self.cursor.execute("UPDATE users SET status = %s WHERE user_id = %s", (True, user_id))
                self.connection.commit()
                return 'success_sub'
            else:
                return 'sub_user'

    def subscription_exist(self, user_id):
        with self.connection:
            self.cursor.execute(f"SELECT status FROM users WHERE user_id = {user_id}")
            result = self.cursor.fetchone()
            return bool(result)

    def set_state(self, user_id, state):
        with self.connection:
            self.cursor.execute(
                "UPDATE users SET state = %s WHERE user_id = %s",
                (state, user_id))
            self.connection.commit()

    def check_state(self, user_id):
        with self.connection:
            self.cursor.execute(f"SELECT state FROM users WHERE user_id = {user_id}")
            state = self.cursor.fetchone()
            return state

    def get_opponent_id(self, user_id):
        with self.connection:
            self.cursor.execute(f"SELECT opponent_id FROM users WHERE user_id = {user_id}")
            opponent_id = self.cursor.fetchone()
            return opponent_id

    def set_opponent_id(self, user_id, opponent_id):
        with self.connection:
            self.cursor.execute(
                "UPDATE users SET opponent_id = %s WHERE user_id = %s",
                (opponent_id, user_id)
            )
            self.connection.commit()

    def get_latest_letter(self, user_id):
        with self.connection:
            self.cursor.execute(f"SELECT last_letter FROM users WHERE user_id = {user_id}")
            latest_letter = self.cursor.fetchone()
            return latest_letter

    def set_latest_letter(self, user_id, latest_letter):
        with self.connection:
            self.cursor.execute(
                "UPDATE users SET last_letter = %s WHERE user_id = %s",
                (latest_letter, user_id))
            self.connection.commit()

    def update_latest_places(self, user_id, place):
        with self.connection:
            self.cursor.execute(f"SELECT latest_places FROM users WHERE user_id = {user_id}")
            latest_places = self.cursor.fetchone()
            temp = latest_places[0]
            if temp == None:
                new_latest_places = [place]
            elif place in temp:
                i = temp.index(place)
                temp.pop(i)
                new_latest_places = [place, *temp]
            else:
                new_latest_places = [place, *temp]
            new_latest_places = new_latest_places[0:4]
            self.cursor.execute(
                "UPDATE users SET latest_places = %s WHERE user_id = %s",
                (new_latest_places, user_id)
            )
            self.connection.commit()

    def get_latest_places(self, user_id):
        with self.connection:
            self.cursor.execute(f"SELECT latest_places FROM users WHERE user_id = {user_id}")
            latest_places = self.cursor.fetchone()
            if latest_places[0] is None:
                return []
            return latest_places[0][:5]

    def get_opponents(self):
        with self.connection:
            self.cursor.execute(f"SELECT user_id FROM users WHERE state = {3}")
            opponents_list = self.cursor.fetchall()
            return opponents_list

    def update_used_places(self, user_id, place):
        with self.connection:
            self.cursor.execute(f"SELECT used_cities FROM users WHERE user_id = {user_id}")
            latest_places = self.cursor.fetchone()
            if latest_places[0] is None:
                self.cursor.execute(
                    "UPDATE users SET used_cities = %s WHERE user_id = %s",
                    ([place], user_id)
                )
                self.connection.commit()
            else:
                self.cursor.execute(
                    "UPDATE users SET used_cities = %s WHERE user_id = %s",
                    ([*latest_places[0], place], user_id)
                )
                self.connection.commit()

    def get_used_places(self, user_id):
        with self.connection:
            self.cursor.execute(f"SELECT used_cities FROM users WHERE user_id = {user_id}")
            latest_places = self.cursor.fetchone()
            if latest_places[0] is None:
                return []
            return latest_places[0]

    def clear_used_places(self, user_id):
        with self.connection:
            self.cursor.execute(
                "UPDATE users SET used_cities = %s WHERE user_id = %s",
                (None, user_id))
            self.connection.commit()
