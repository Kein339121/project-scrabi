from fastapi import (
    FastAPI,
    Query,
    Request
)
from initialization import (
    db,
    bot
)
from message_hendler import (
    validate_message,
    send_weather,
    sub_unsub,
    cities_game,
    find_opponent,
    accept_opponent,
    draw_accept
)


import json

app = FastAPI()


@app.get('/')
async def home():
    return {'ОГО': 'ОНО РАБОТАЕТ'}


@app.post('/bot')
async def bot_polling(request: Request):
    response = await request.body()
    response = response.decode('utf-8')
    json_response = json.loads(response)
    chat_id = text = name = ' '
    try:
        chat_id = json_response['message']['chat']['id']
        text = json_response['message']['text']
        name = json_response['message']['chat']['first_name']
        text_message = True
    except:
        text_message = False
    if text_message:
        if db.user_exist(chat_id):
            state = db.check_state(chat_id)[0]
            if state == 1:
                send_weather(chat_id, text)
            elif state == 2:
                cities_game(chat_id, text)
            elif state == 3:
                find_opponent(chat_id, text)
            elif state == 4:
                accept_opponent(chat_id, text)
            elif state == 6:
                draw_accept(chat_id, text)
            else:
                validate_message(chat_id, text.lower(), name)

        else:
            db.add_user(chat_id)
            validate_message(chat_id, text.lower(), name)
    return {"received_request_body": json_response}


@app.get('/add_user')
async def add_user(
        user_id: str = Query(
            None,
            min_length=8,
            max_length=10
        ),
        type_request: str = None
):
    result = db.add_user(user_id)
    if type_request is None:
        result = result + '_api'
    sub_unsub(user_id, result)
    return {'user added': f'user id:\t{user_id}'}


@app.get('/remove_user')
async def remove_user(
        user_id: str = Query(
            None,
            min_length=8,
            max_length=10
        ),
        type_request: str = None
):
    result = db.remove_user(user_id)
    if type_request is None:
        result = result + '_api'
    sub_unsub(user_id, result)
    return {'user removed': f'user id:\t{user_id}'}


@app.get('/subscribe_user')
async def subscribe_user(
        user_id: str = Query(
            None,
            min_length=8,
            max_length=10
        ),
        type_request: str = None
):
    result = db.set_subscription(user_id)
    if type_request is None:
        result = result + '_api'
    sub_unsub(user_id, result)
    return {'user added': f'user id:\t{user_id}'}


@app.get('/unsubscribe_user')
async def unsubscribe_user(
        user_id: str = Query(
            None,
            min_length=8,
            max_length=10),
        type_request: str = None
):
    result = db.remove_subscription(user_id)
    if type_request is None:
        result = result + '_api'
    sub_unsub(user_id, result)
    return {'user removed': f'user id:\t{user_id}'}


@app.get('/send_message')
async def send_message(
        message: str = Query(
            None,
            min_length=1)
):
    users = db.return_users()
    api_message = "⚠️Это системное сообщение⚠️\n" + message
    for user_id in users:
        bot.send_message(user_id, api_message)
    return {'send message': users}
