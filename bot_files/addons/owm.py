from pyowm import OWM
from pyowm.utils.config import get_default_config


class OpenWeatherMap:
    def __init__(self, token):
        self.token = OWM(token)
        self.mgr = self.token.weather_manager()
        self.config_dict = get_default_config()
        self.config_dict['language'] = 'ru'

    def check_city(self, city):
        try:
            self.mgr.weather_at_place(city)
            return True
        except:
            return False

    def return_weather(self, city):
        observation = self.mgr.weather_at_place(city)
        w = observation.weather
        rains = w.rain
        if len(rains) == 0:
            rains = 0
        else:
            rains = rains['1h'] * 100
        return (
            w.detailed_status,
            w.wind()['speed'],
            w.humidity,
            w.temperature('celsius')['temp'],
            w.temperature('celsius')['feels_like'],
            rains
        )
