import requests


def build_message(resp):
    massege = 'Курс покупки валют на сегодня:\n'
    for keys in resp.keys():
        if keys == 'USDRUB':
            massege += f'Курс рубля к доллару: {resp["USDRUB"]} руб.\n'
        if keys == 'EURRUB':
            massege += f'Курс рубля к евро: {resp["EURRUB"]} руб.\n'
        if keys == 'GBPRUB':
            massege += f'Курс рубля к фунту стерлингов: {resp["GBPRUB"]} руб.\n'
        if keys == 'UAHRUB':
            massege += f'Курс рубля к гривне: {resp["UAHRUB"]} руб.\n'
        if keys == 'JPYRUB':
            massege += f'Курс рубля к ене: {resp["JPYRUB"]} руб.\n'
        if keys == 'CNYRUB':
            massege += f'Курс рубля к юаню: {resp["CNYRUB"]} руб.\n'
    return massege


def pars_of_valut():
    response = requests.get(f'https://currate.ru/api/?get=rates&pairs=USDRUB,UAHRUB,GBPRUB,EURRUB,JPYRUB,CNYRUB&'
                            f'key=43de0b7990ad51188e5a29df8753154a')
    resp=response.json()
    message = build_message(resp['data'])
    return message

