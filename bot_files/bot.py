import requests


class Bot:
    def __init__(self, token, api_server=None, data_base=None):
        self.token = token
        self.url = f'https://api.telegram.org/bot{token}/'
        self.api = api_server
        self.db = data_base

    def set_webhook(self):
        webhook = self.url + "setWebhook?url=" + self.token
        requests.get(webhook)

    def send_message(self, chat_id, text, state=0, keyboard=None):
        message = {
            "chat_id": chat_id,
            "text": text,
        }
        if keyboard is not None:
            message.update(keyboard)
        print(message)
        url = self.url + 'sendMessage'
        response = requests.post(url, json=message)
        if self.db is not None:
            self.db.set_state(chat_id, state)
        return response.json()

    def add_user(self, user_id):
        requests.get(f'{self.api}/add_user?user_id={user_id}&type_request=bot')

    def sub_user(self, user_id):
        requests.get(f'{self.api}/subscribe_user?user_id={user_id}&type_request=bot')

    def unsub_user(self, user_id):
        requests.get(f'{self.api}/unsubscribe_user?user_id={user_id}&type_request=bot')

