MESSAGES = {
    'weather': 'Погода в городе {place}\n'
               'Tучи: {clouds}\n'
               'скорость ветра: {wind_speed}м/с\n'
               'Влажность: {hum}%\n'
               'Температура: {temp}, ощущается как: {f_like}\n'
               'Вероятность дождя в ближайший час: {rains}%',
    'wrong_city': 'Вы ошиблись в названии города, попробуйте еще раз',
    'success_unsub': 'Вы успешно отписались',
    'not_sub_user': 'Вы и так не подписаны',
    'success_sub': 'Вы успешно подписались',
    'sub_user': 'Вы и так подписанны',
    'default': 'Вы перешли в стардартный режим эхо бота',
    'success_add_api': '⚠️Вас добавили в базу через админскую панель⚠️',
    'existed_user_api': '⚠️Вас пытались добавить в базу через админскую панель, '
                        'но вы и так подписаны⚠️',
    'not_existed_user_api': '⚠️Вас пытались убрать из базы через админскую панель, '
                            'но вы и так не подписаны⚠️',
    'success_remove_api': '⚠️Вас убрали из базы через админскую панель⚠️',
    'success_unsub_api': '⚠️Вас успешно отписали через админскую панель⚠️',
    'not_sub_user_api': '⚠️Вас пытались отписать через админскую панель, '
                        'но вы и так не подписаны⚠️',
    'success_sub_api': '⚠️Вас успешно подписали через админскую панель⚠️',
    'sub_user_api': '⚠️Вас пытались подписать через админскую панель, '
                    'но вы и так подписаны⚠️',
    'wrong_opponent': 'Оппонент не в находиться в подборе игры, '
                      'если это ваш друг, '
                      'попросите его зайти в игру командой /cities',
    'wait_opponent': 'Ожидание ответа опонента',
    'oppoent_wait': 'Игрок {opponent_id}, приглашает вас в игру.\n'
                    'Выберите действие:\n'
                    'принять вызов - начало игры\n'
                    'отказаться - ждать другого опонента\n'
                    'назад - возвращение в главное меню',
    'game_start': 'Игра против игрока {opponent} началась.\n'
                  'Первый ход за {first}',
    'draw': 'Ваш оппонент предлагает ничью\n'
            'выберите один из пунктов ниже',
    'draw_accepted': 'Ничья, ваш счет - {score}',
    'draw_delay': 'Соперник отказалася от ничьей - игра продолжается',
    'draw_wrong': 'Вы выбрали не существующий пункт',
    'used': 'Город {city} уже был использован {by_who}',
    'correct_city': 'Город {city} зачтен, ваш счет - {score}',
    'wrong_letter': 'Вы указали город не на ту букву,'
                    ' нужная буква - {last_letter}',
    'you_win': 'Ваш опонент сдался и вы победили,'
               ' ваш счет - {score}',
    'you_loose': 'Вы сдались, ваш счет - {score}',
    'your_turn': 'Город оппонента - {city}, ваш ход',
    'opponent_skiped': 'Оппонент отклонил ваш вызов, вы вернулись в режим выбора оппонента\n'
                       'Выберите ID соперника из списа на всплывающей клавиатуре,'
                       'укажите ID своего друга вручную или '
                       'подождите приглашения от другого игрока',

}
