def array_of_arrays(lst):
    res = []
    for el in lst:
        sub = el.split(', ')
        res.append(sub)
    return res


def city_keyboards(cities=None):
    if cities is not None:
        keyboard = [*array_of_arrays(cities), ['назад']]
    else:
        keyboard = [
            ['назад']
        ]
    return {
        "reply_markup": {
            "keyboard": keyboard,
            "one_time_keyboard": True
        }
    }


def opponent_keiboards(chat_id, opponents: list):
    if opponents is not None:
        temp = []
        for opponent in opponents:
            if opponent[0] != chat_id:
                temp.append(str(opponent[0]))
        keyboard = [*array_of_arrays(temp), ['назад']]
    else:
        keyboard = [
            ['назад']
        ]
    return {
        "reply_markup": {
            "keyboard": keyboard,
            "one_time_keyboard": True
        }
    }


def opponent_wait_keyboard(opponent_id):
    return {
        "reply_markup": {
            "keyboard": [
                ['принять вызов'],
                ['отказаться'],
                ['назад']
            ],
            "one_time_keyboard": True
        }
    }

def city_game_keyboard():
    return {
        "reply_markup": {
            "keyboard": [
                ['сдаться'],
                ['ничья']
            ],
            "one_time_keyboard": True
        }
    }


def draw_keyboard():
    return {
        "reply_markup": {
            "keyboard": [
                ['ничья'],
                ['отмена']
            ],
            "one_time_keyboard": True
        }
    }
