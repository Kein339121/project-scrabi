from initialization import bot
from initialization import owm
from initialization import db
from bot_files.keyboard_parser import city_keyboards
from bot_files.comands import COMMANDS
from bot_files.mesages import MESSAGES


def send_weather(chat_id, text):
    if text.lower() == 'назад':
        answer = MESSAGES['default']
        state = 0

    elif owm.check_city(text):
        db.update_latest_places(
            user_id=chat_id,
            place=text
        )
        clouds, wind_speed, hum, temp, f_like, rains = owm.return_weather(text)
        answer = MESSAGES['weather'].format(
            place=text,
            clouds=clouds,
            wind_speed=wind_speed,
            hum=hum,
            temp=temp,
            f_like=f_like,
            rains=rains
        )
        state = 0
    else:
        answer = MESSAGES['wrong_city']
        state = 1
    bot.send_message(
        chat_id=chat_id,
        text=answer,
        state=state
    )


def cities_game(chat_id, text):
    pass


def message_handler(chat_id, text):
    if text[(len(text)-4):] == "_api":
        bot.send_message(
            chat_id=chat_id,
            text=MESSAGES[text]
        )
    else:
        bot.send_message(
            chat_id=chat_id,
            text=text[::-1],
        )


def command_handler(chat_id, text, name):
    if text == '/start':
        db.add_user(chat_id)
        bot.send_message(
            text=COMMANDS[text].format(name=name),
            chat_id=chat_id
        )
    elif text == '/sub':
        bot.sub_user(user_id=chat_id)
    elif text == '/unsub':
        bot.unsub_user(user_id=chat_id)
    elif text == '/weather':
        cities = db.get_latest_places(chat_id)
        keyboard = city_keyboards(cities)
        if len(cities) != 0:
            text = COMMANDS[text+'1']
        else:
            text = COMMANDS[text]
        bot.send_message(
            text=text,
            chat_id=chat_id,
            keyboard=keyboard,
            state=1
        )
    else:
        bot.send_message(
            text=COMMANDS['wrong'],
            chat_id=chat_id
        )
