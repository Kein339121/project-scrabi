from static import config
from bot_files.bot import Bot
from bot_files.addons.owm import OpenWeatherMap
from db.postgresser import Postgresser


db = Postgresser(config.DB_URI)
bot = Bot(
    token=config.TOKEN,
    api_server=config.API_SERVER,
    data_base=db
)
owm = OpenWeatherMap(config.OWM_API_TOKEN)

